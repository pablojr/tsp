#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "tsp.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

public slots:
    void tspVisualize(const TspData data);
    void on_btnRead_clicked();

private:
    Ui::MainWindow *ui;

    TSP *tsp;
};

#endif // MAINWINDOW_H
