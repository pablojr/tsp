#include "tsp.h"

TSP::TSP(QObject *parent) : QObject(parent)
{

}

TSP::~TSP()
{

}

void TSP::tsp_can()
{
    // do some stuff, grab data
    TspData data;
    data.d1 = 123;
    data.str = "Hello";

    // when ready, tell the world data is available
    emit readingAvailable(data);
}
