#ifndef TSPDATA_H
#define TSPDATA_H

#include <QObject>

struct TspData {
    int d1;
    QString str;
};

Q_DECLARE_METATYPE(TspData)

#endif // TSPDATA_H
