#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    // create TSP object and connect its signal
    tsp = new TSP(this);
    connect(tsp, &TSP::readingAvailable, this, &MainWindow::tspVisualize);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_btnRead_clicked()
{
    tsp->tsp_can();
}

void MainWindow::tspVisualize(const TspData data)
{
    ui->ledtValueA->setText(QString::number(data.d1));
    ui->ledtValueB->setText(data.str);
}
