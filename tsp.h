#ifndef TSP_H
#define TSP_H

#include <QObject>
#include "tspdata.h"

class TSP : public QObject
{
    Q_OBJECT

public:
    explicit TSP(QObject *parent = nullptr);
    ~TSP();

    void tsp_can();

signals:
    void readingAvailable(TspData data);
};

#endif // TSP_H
